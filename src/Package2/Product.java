package Package2;

public class Product implements Taxable{
	private String nameProduct;
	private double price;
	
	public Product(String np, double p){
		nameProduct = np;
		price = p;
	}
	
	public String getName(){
		return nameProduct;
	}
	
	public double getPrice(){
		return price;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return price*0.07;
	}
}
