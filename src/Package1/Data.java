package Package1;

public class Data {
	
	public static double getAverage(Measurable[] objects){
		double sum = 0;
		for (Measurable obj : objects) {
			sum = sum + obj.getMeasure(); 
		}
		if (objects.length > 0){
			return sum / objects.length;
		}else{
			return 0;
		}
	}
	
	public static Measurable Min(Measurable m1, Measurable m2, Measurable m3){
		if (m1.getMeasure() < m2.getMeasure() && m1.getMeasure() < m3.getMeasure()){
			return m1;
		}else if (m2.getMeasure() < m1.getMeasure() && m2.getMeasure() < m3.getMeasure()) {
			return m2;
		}else{
			return m3;
		}	
	}
}
