package Package1;

public class Country implements Measurable{
	private String name;
	private double area;
	
	public Country(String n, double a){
		name = n;
		area = a;
	}
	
	public String getName(){
		return name;
	}
	
	public double getArea(){
		return area;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
}
