package Package1;


public class Main {

	public static void main(String[] args) {
		
	Measurable[] person = new Measurable[3];
	person[0] = new Person("Numsai", 170);
	person[1] = new Person("Waree", 150);
	person[2] = new Person("Somsri", 160);
	
	double averageHeight = Data.getAverage(person);
	System.out.println("Average Height : " + averageHeight);
	

	Person num = new Person("Numsai", 170);
	Person wa = new Person("Waree", 150);
	Person som = new Person("Somsri", 160);
		
	Measurable minheight = Data.Min(num, wa, som);
	Person per = (Person) minheight;
	double height = per.getHeight();
	System.out.println("Minimum Height : " + height);
	
	Country eng = new Country("England", 7000000);
	Country chi = new Country("China", 9000000);
	Country jap = new Country("Japan", 500000);
	
	Measurable mincountry = Data.Min(eng, chi, jap);
	Country count = (Country) mincountry;
	double minarea = count.getArea();
	System.out.println("Minimum Area : " + minarea);
	
	BankAccount nina = new BankAccount("Nina", 7200);
	BankAccount manee = new BankAccount("Manee", 3000);
	BankAccount upin = new BankAccount("Upin", 2500);
	
	Measurable minbank = Data.Min(nina, manee, upin);
	BankAccount bank = (BankAccount) minbank;
	double minBalance = bank.getBalance();
	System.out.println("Minimum Balance : " + minBalance);
	
	
	}
}
