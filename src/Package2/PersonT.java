package Package2;

public class PersonT implements Taxable{
	private String name;
	private double income;
	
	public PersonT(String n, double ic){
		name = n;
		income = ic;
	}
	
	public String getName(){
		return name;
	}
	
	public double getIncome(){
		return income;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double sum,sum1,result = 0;
		if (income > 0 && income <= 300000){
			sum = income*0.05;
			return sum;
		}else{
			sum = income-300000;
			sum1 = income-sum;
			result = (sum*0.10)+(sum1*0.05);
			return result;
		}
	}
}
