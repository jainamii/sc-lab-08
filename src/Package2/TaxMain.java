package Package2;

import java.util.ArrayList;



public class TaxMain {
	
	public static void main(String[] args) {	
		
		ArrayList<Taxable> person = new ArrayList<Taxable>();
		person.add(new PersonT("Wi", 500000));
		person.add(new PersonT("Sara", 300000));
		person.add(new PersonT("Rat", 75000));
		
		double sumTaxPerson = TaxCalculator.sum(person);
		System.out.println("Sum Tax Person : " + sumTaxPerson);
		
		ArrayList<Taxable> company = new ArrayList<Taxable>();
		company.add(new Company("Sea Fly Services Company Limited", 8000000, 2000000));
		company.add(new Company("Earth Telecom co.,Ltd", 2000000, 500000));
		company.add(new Company("Wong-sa-whang corporation public company limited", 10000000, 8000000));
		
		double sumTaxCompany = TaxCalculator.sum(company);
		System.out.println("Sum Tax Company : " + sumTaxCompany);
		
		ArrayList<Taxable> product = new ArrayList<Taxable>();
		product.add(new Product("Rotring", 199));
		product.add(new Product("Table", 2075));
		product.add(new Product("Paper A4", 560));
		
		double sumTaxProduct = TaxCalculator.sum(product);
		System.out.println("Sum Tax Product : " + sumTaxProduct);
	
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new PersonT("Sucha", 500000));
		tax.add(new Company("Sara Crop International co.,Ltd", 1000000, 800000));
		tax.add(new Product("Soup", 100));

		double sumTax = TaxCalculator.sum(tax);
		System.out.println("Sum Tax : " + sumTax);
		
	}
}
