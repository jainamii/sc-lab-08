package Package1;

public class Person implements Measurable{
	private String name;
	private double height;
	
	public Person(String n, double h){
		name = n;
		height = h;
	}
	
	public String getName(){
		return name;
	}
	
	public double getHeight(){
		return height;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return height;
	}
}
